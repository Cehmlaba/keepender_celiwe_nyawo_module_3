import 'package:flutter/material.dart';

class FeatureScreen1 extends StatelessWidget {
  const FeatureScreen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: (const Text(
          "WELCOME TO MY APP",
          textAlign: TextAlign.center,
        )));
  }
}
