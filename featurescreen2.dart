import 'package:flutter/material.dart';

class FeatureScreen2 extends StatelessWidget {
  const FeatureScreen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: (const Text(
          "please share our link",
          textAlign: TextAlign.center,
        )));
  }
}
